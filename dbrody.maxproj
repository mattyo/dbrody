{
	"name" : "db_pierogi",
	"version" : 1,
	"creationdate" : -815528643,
	"modificationdate" : -814989356,
	"viewrect" : [ 25.0, 69.0, 300.0, 500.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"db_pierogi.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}

		}
,
		"media" : 		{
			"audio.aif" : 			{
				"kind" : "audiofile",
				"local" : 1
			}
,
			"movie1.aif" : 			{
				"kind" : "audiofile",
				"local" : 1
			}
,
			"movie2.mov" : 			{
				"kind" : "moviefile",
				"local" : 1
			}

		}
,
		"data" : 		{

		}
,
		"externals" : 		{

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0
}
